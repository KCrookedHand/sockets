package com.chat.client;

public interface ClientEventChat {
	void messageReceived(String msg);

	void disconnectedFromServer();
}
